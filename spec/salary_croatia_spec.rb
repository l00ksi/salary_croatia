RSpec.describe SalaryCroatia do
  
  before :all do
  	filename = "dzs.xlsx"
  	download_url = "http://www.dzs.hr/Hrv_Eng/Pokazatelji/Osnovni%20pokazatelji.xlsx"
  	File.open(filename, "wb") do |saved_file|
		open(download_url, "rb") do |read_file|
			saved_file.write(read_file.read)
		end
	end
  end

  it "has a version number" do
  	puts "Version: " + SalaryCroatia::VERSION
    expect(SalaryCroatia::VERSION).not_to be nil
  end

  it "gets latest salary number" do
    puts "HIH"
  	puts "Latest salary: " + WageStats.get_latest_average_salary.to_s
  	expect(WageStats.get_latest_average_salary).to be > 5000
  end

  it "gets salary for date with line" do 
  	date = "01-05-2005"
  	date_salary = WageStats.get_salary_for_date(date)
  	puts "Salary for(" + date + ") is: " + date_salary.to_s
  	expect(date_salary).not_to be nil and expect(date_salary.class).to eq(Integer)
  end

  it "gets salary for date with empty space" do 
  	date = "21 09 2015"
  	date_salary = WageStats.get_salary_for_date(date)
  	puts "Salary for(" + date + ") is: " + date_salary.to_s
  	expect(date_salary).not_to be nil and expect(date_salary.class).to eq(Integer)
  end

  it "gets salary for date with inversed date and year" do 
  	date = "2018-05-05"
  	date_salary = WageStats.get_salary_for_date(date)
  	puts "Salary for(" + date + ") is: " + date_salary.to_s
  	expect(date_salary).not_to be nil and expect(date_salary.class).to eq(Integer)
  end



  it "Number of dates equals number of salaries" do
  	merged_hash = WageStats.merge_arrays
  	expect(merged_hash.instance_of?(Hash)).to eq(true)
  end

  it "Data source file exists?" do 
  	expect(File.exists?("dzs.xlsx")).to eq(true)	
  end


end
