# In this file converting dates will be conducted e.g. date provided in the xlsx file from www.dzs.hr is in month+year format that looks like ["I. 2004.", "II. 2004.", "IX. 2016."...]
require 'date'

class ConvertDate

	def get_dates(dates_array = [])
		formatted_dates = 	dates_array.map do |date|
								get_date(date)
							end
	end

	def get_date(date)
		start_string 	= date.split(" ")[0]
		end_string		= date.split(" ")[1]	

		helper_hash = {
			"I."	=> "01.",
			"II."	=> "02.",
			"III."	=> "03.",
			"IV."	=> "04.",
			"V."	=> "05.",
			"VI."	=> "06.",
			"VII."	=> "07.",
			"VIII."	=> "08.",
			"IX."	=> "09.",
			"X."	=> "10.",
			"XI."	=> "11.",
			"XII."	=> "12."
		}

		"01." + helper_hash[start_string] + end_string
	end

	# transform date considering the type of input -> ((month + year) && (day + month + year)) ALLOWED!
	def transform_date(date)
		delimiter = date.tr("0-9", "")[0]	# Removing digits and returning first non-numeric character
		date_array = date.split(delimiter)	# Splitting date input by its delimiter
		if date_array.size > 2
			first_element = date_array[0]
			date_array.delete_at(date_array.find_index(first_element))
		end

		return "01." + date_array.join(".") + "."
	end

end