class DataTypeHelper

	def self.merge_arrays_into_hash(array1, array2)
		hash = {}
		array1.each_with_index do |key, index|
			hash[key] = array2[index]
		end
		return hash
	end

end