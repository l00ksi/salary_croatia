require 'open-uri'
require 'creek'
require_relative './salary_croatia/convert_date'
require_relative './salary_croatia/data_type_helper'

WAGE_STATISTICS_URL ||= "http://www.dzs.hr/Hrv_Eng/Pokazatelji/Osnovni%20pokazatelji.xlsx"
FILE_NAME ||= "dzs.xlsx"

class WageStats
	attr_accessor :file_name, :download_url
	@@average_salary_hash = {}
	@@dates_hash = {}
	@@merged_hash = {}


	# class method, that is using default values for download and file_name
	def self.update_stats(filename=FILE_NAME, download_url=WAGE_STATISTICS_URL)
		if !File.file?(filename) or download_url != WAGE_STATISTICS_URL
			File.open(filename, "wb") do |saved_file|
				open(download_url, "rb") do |read_file|
					saved_file.write(read_file.read)
				end
			end
			#puts "Updated stats!"
		end

		if (@@average_salary_hash.empty?)
			file_data = Creek::Book.new filename
			file_data.sheets[0].rows.each_with_index do |row, index|
				@@average_salary_hash = row if index == 75
				@@dates_hash = row if index == 6
			end
		end
	end

# ------------------------------------------------------------------------------
# Basically last month value of average national net salary
	def self.get_latest_average_salary
		if File.exists?(FILE_NAME)
			self.update_stats
		end

		if @@merged_hash.empty?
			self.update_stats
			self.merge_arrays
		end
		@@average_salary_hash.values.last
	end

# ------------------------------------------------------------------------------

	def self.get_salary_for_date(date)
		if @@merged_hash.empty?
			self.update_stats
			self.merge_arrays
		end
		converted_date = ConvertDate.new.transform_date(date)
		return @@merged_hash[converted_date.to_s]
	end

# ------------------------------------------------------------------------------

	def self.get_year_of_salaries(year)
		if @@merged_hash.empty?
			self.update_stats
			self.merge_arrays
		end
		year_salaries = []
		@@merged_hash.values.each_with_index do |value, index|
			if @@merged_hash.keys[index].include?(year) and year.length > 3
				year_salaries << value
			end
		end

		return year_salaries
	end
# ------------------------------------------------------------------------------



	def self.get_salary_history
		if @@merged_hash.empty?
			self.update_stats
			self.merge_arrays
		end
		return @@merged_hash
	end

# ------------------------------------------------------------------------------

	def self.merge_arrays
		dates_array 	= self.convert_dates_array(@@dates_hash.values.drop(2))
		salaries_array 	= @@average_salary_hash.values.drop(2)		# Removing 2 empty cells from the start of an array
		while dates_array.size > salaries_array.size
			dates_array.pop
		end

		if dates_array.size == salaries_array.size
			@@merged_hash = DataTypeHelper.merge_arrays_into_hash(dates_array, salaries_array)
		else
			puts "Dates size: " + dates_array.size.to_s
			puts dates_array
			puts "Salary size: " + salaries_array.size.to_s
			puts salaries_array
			raise Exception.new "Arrays have different size, thus incompatible"
		end
	end 

# ------------------------------------------------------------------------------

	def self.convert_dates_array(dates_array)		
		cd = ConvertDate.new
		cd.get_dates(dates_array)
	end

end